/*
 * app_selector_ejercicio.c
 *
 *  Created on: Aug 10, 2021
 *      Author: developer
 */


#include "inc/guia_1_ejercicios.h"
#include "inc/guia_2_ejercicios.h"
#include "inc/guia_3_ejercicios.h"

#define EJERCICIO_1A 1
#define EJERCICIO_1B 2
#define EJERCICIO_21 3
#define EJERCICIO_22 4
#define EJERCICIO_23 5
#define EJERCICIO_31 6
#define EJERCICIO_32 7


#define TEST_EJERCICIO EJERCICIO_32

int main () {


#if TEST_EJERCICIO == EJERCICIO_1A
	ejercicio_1A_guia_1();

#elif TEST_EJERCICIO == EJERCICIO_21
	ejercicio_21_guia_1();

#elif TEST_EJERCICIO == EJERCICIO_22
	ejercicio_22_guia_1();

#elif TEST_EJERCICIO == EJERCICIO_23
	ejercicio_23_guia_1();

#elif TEST_EJERCICIO == EJERCICIO_31
	ejercicio_31_guia_1();

#elif TEST_EJERCICIO == EJERCICIO_32
	ejercicio_32_guia_1();

#endif


}
