/*
 * guia_2_ejercicios.c
 *
 *  Created on: Aug 10, 2021
 *      Author: developer
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MODO_MENOR_MAYOR_ABS	1
#define MODO_MENOR_MAYOR		2
#define MODO_MAYOR_MENOR_ABS	3
#define MODO_MAYOR_MENOR		4

/**
 * @brief Compare function
 */
typedef int (*CompareFunc)(float a, float b);


/* -------------- ARREGLOS   --------------*/

void print_array_to_stdout(int* vector, int size ){
	unsigned int i;
	printf("Array = [");

	for (i = 0; i < size; ++i) {
		printf("%d, ", vector[i]);
	}
	printf("]\n");
	return;
}

void ordenamiento_array( int* vector, int size, CompareFunc mifuncion){

}

/* Ejercicio 1
 * El programa será del tipo conversacional (interactivo) donde se podrá realizar cualquiera de las siguientes operaciones:
 * 1. Ordenamiento de menor a mayor en valor absoluto.
 * 2. Ordenamiento de menor a mayor algebraicamente (con signo).
 * 3. Ordenamiento de mayor a menor en valor absoluto.
 * 4. Ordenamiento de mayor a menor algebraicamente (con signo).
 *
 */

void ejercicio_1_guia_2(void)
{
	int opcion, vectorTest[]= {4.7, -8.0, -2.3, 11.4, 12.9, 5.1, 8.8, -0.2, 6.0, -14.7};
	char formato[2];
	// Podria predefinir la longitud del vector pero lo dejo a modo de ejemplo:
	size_t nVector = sizeof(vectorTest) / sizeof(int);

	printf("--- Programa Ordenamiento de Vectores ---\n \
			El programa utiliza el siguiente vector de prueba:\n ");
	print_array_to_stdout(vectorTest, (int)nVector );
	printf("Por favor ingrese el modo de oredenamiento:\n \
			1 -> Menor a Mayor absoluto.\n \
			2 -> Menor a Mayor con signo.\n \
			3 -> Mayor a Menor absoluto.\n \
			4 -> Mayor a Menor con signo.\n ");
	scanf( "%2s" , formato);

	/* OJO lo uso a conciencia del riesgo:
	 * On success, the function returns the converted integral number as an int value.
	 * If the converted value would be out of the range of representable values by an int, it causes undefined behavior.
	 * See strtol for a more robust cross-platform alternative when this is a possibility.
	 */
	opcion= atoi(formato);

	switch (opcion) {
		case MODO_MENOR_MAYOR_ABS:

			break;
		case MODO_MENOR_MAYOR:

			break;
		case MODO_MAYOR_MENOR_ABS:

			break;
		case MODO_MAYOR_MENOR:

			break;
		default:
			break;
	}
	return;
}


/* Ejercicio 3
 * Escribir un programa que simule un juego de blackjack entre dos jugadores. La computadora no será un participante
 * en el juego, simplemente dará las cartas a cada jugador y proveerá a cada jugador con una o más cartas adicionales
 * cuando este la solicite.
 */
#define MAX_CARTAS_PLAY   6
#define NUMERO_PLAYERS    2

typedef struct {
	char numbre[15];
	int puntos;
	int cantNaipes;
	int Naipes[MAX_CARTAS_PLAY];

} blackPlay_t;

void ejercicio_3_guia_2(void)
{
	blackPlay_t players[NUMERO_PLAYERS]={{"Juan",0,2},{"Pablo",0,2}};
	blackPlay_t *ptrPlayer= &players;
	int iPlayer=0, activePlayer=NUMERO_PLAYERS, endPlay=0;
	char reply;

	/* initialize random seed: */
	srand (time(NULL));

	printf(" ----- Bienvenido al Blackjack ----- \n");
	// 1- Se reparten las dos primeras cartas a cada jugador y calculan los puntos.
	// generate secret number between 1 and 13 (1,..,10,J,Q,K):
	ptrPlayer->Naipes;

	entregarCarta(players[0]);
	entregarCarta(players[1]);
	entregarCarta(players[0]);
	entregarCarta(players[1]);
	players[0].Naipes[0]= rand()%13 + 1;

	calculoPuntos(players[0]); // Puede entrar adentro de entregarCarta()
	calculoPuntos(players[1]);

	do{
		printf(" Jugador %s estos son sus cartas y sus puntos: \n", players[iPlayer].numbre);
		mostrarNaipes( players[iPlayer] );
		do{
			printf(" Desea una carta mas (S) o se planta (N) ?... \n");
			scanf( "%1c" , &reply);

			switch (reply) {
				case 'S':
					entregarCarta( players[iPlayer] );
					mostrarNaipes( players[iPlayer] );
					break;
				case 'N':
					endPlay=1;
					break;
				default:
					printf(" Ingrese (S) para pedir otra carta o (N) si desea plantarse. \n");
					break;
			}

		} while( endPlay == 0 );

		endPlay=0;
		activePlayer--;
	} while( activePlayer == 0 );

	return;
}












