#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/* -------------- ENTRADA Y SALIDA DE DATOS   --------------*/

void mostrar_variable_cd(double nprint) {
	printf("La variable tiene la siguiente resolucion: %#e14.8e \n", nprint );

	return;
}
void mostrar_variable_e(double nprint) {
	printf("La variable tiene la siguiente resolucion: %#e14.8e \n", nprint );

	return;
}

void mostrar_variable_f(double nprint) {
	printf("La variable tiene la siguiente resolucion: %#e14.8e \n", nprint );

	return;
}

int 	i = 102;
int 	j = -56;
float 	x = 12.687;
double 	dx = 0.000000025;

long 	ix = -158693157400;
unsigned u = 35460;
char 	c = 'C';

/* Ejercicio 1.A:
 * Escribir los valores de i, j, x y dx suponiendo que cada cantidad entera tiene una {longitud de campo mínima de
 * 4 caracteres} y cada cantidad en coma flotante se presenta en {notación exponencial} con un total de {al menos
 * 14 caracteres} y no más de {8 cifras decimales}.
 */
void mostrar_variable_ab(double nprint) {
	printf("La variable tiene la siguiente resolucion: %#e14.8e \n", nprint );

	return;
}

void ejercicio_1A_guia_1 (void) {
	mostrar_variable_ab(i);
	mostrar_variable_ab(j);
	mostrar_variable_ab(x);
	mostrar_variable_ab(dx);
}

/* Ejercicio 1.C:
 * Escribir los valores de i, ix, j, x y u suponiendo que cada cantidad entera tendrá una {longitud de campo mínima
 * de 5 caracteres}, el entero largo tendrá una {longitud de campo mínima de 12 caracteres} y la cantidad en coma
 * flotante tiene {al menos 10 caracteres con un máximo de 5 cifras decimales}. No incluir el exponente.
 * D: Repetir C, visualizando las tres primeras cantidades en una línea, seguidas de una línea en blanco y las otras
 * dos cantidades en la línea siguiente.
 */

/* Ejercicio 1.E:
 * Escribir los valores de i, u y c, con una longitud de campo {mínima de 6 caracteres} para cada cantidad entera.
 * Separar con {3 espacios en blanco} cada cantidad.
 */

/* Ejercicio 1.F:
 * Escribir los valores de j, u y x. Visualizar las 3 cantidades enteras con una {longitud de campo mínima de
 * 5 caracteres}. Presentar la cantidad en coma flotante utilizando la conversión tipo f, con una {longitud
 * mínima de 11 caracteres} y un {máximo de 4 cifras} decimales.
 */


/* Ejercicio 2.X
 * Escribir las instrucciones scanf o printf necesarias para cada uno de los siguientes puntos:
 *  Ej 2.1, Ej 2.2, Ej 2.3
 */
void ejercicio_21_guia_1(void) {
	char nombre[20];
	printf("Por favor ingrese su nombre:\n");
	scanf( "%20s" , nombre);

	printf("Saludos %s", nombre );
	return;
}

void ejercicio_22_guia_1(void) {

	float x1=8.0, x2=-2.5;
	printf("Las variables punto flotante almacenadas son:\n");
	printf( "x1 = %1.1f \n" , x1);
	printf( "x2 = %1.1f \n" , x2);

	return;
}

void ejercicio_23_guia_1(void) {
	long int val1=0, val2=0;

	printf("Por favor ingrese la dos valores a sumar (maximo 10 caracteres enteros):\n");
	scanf( "%10li" , &val1);
	printf("Valor1 = %li\n", val1);
	scanf( "%10li" , &val2);
	printf("Valor2 = %li\n", val2);

	printf("Resultado = %li \n. Graciasssss\n", val1+val2 );

	return;
}
/* -------------- INSTRUCCIONES DE CONTROL   --------------*/
/*
 * Ejercicio 3.1
 * Escribir un bucle for o while que lea los caracteres de una cadena de caracteres llamada texto y los escriba en
 * sentido opuesto en otra cadena llamada inverso.
 * Suposiciones: 1-la cadena texto {contendrá 80 caracteres} como máximo, pero la longitud del texto será determinado
 * en tiempo de ejecución, dependiendo de la cadena que ingrese el usuario. 2- Utilizar el operador coma dentro del
 * bucle for (while). 3- Utilizar las funciones scanf (o fgets) y printf para obtener y mostrar los datos respectivamente.
 * 4- El programa finalizará solamente cuando el usuario ingrese “-1” por teclado.
 */
#define SIZE_TEXTO 81
#define ESCAPE_CMD "-1"

void ejercicio_31_guia_1(void) {
	char texto[SIZE_TEXTO], *ptrstart, *ptrend, auxchar;
	int buffsize;

	while(1)
	{
		printf("Por favor ingrese una palabra no mayor a 80 caracteres o (-1) para salir:\n");

		ptrstart= fgets(texto, SIZE_TEXTO, stdin);
		buffsize= strlen(texto);
		ptrend= texto+buffsize-1;  // Descuento el [\n\0] que se insertan al final de la cadena leida.
		*ptrend='\0', ptrend--;
		if( ptrstart==NULL || !strcmp(ESCAPE_CMD, texto) ){
			//system("clear"); No es una buena opcion ya que depende de la implementacion y no se ejecuta bien en Eclipse.
			printf("Gracias por usar al soft inversor!!! \n");
			break;
		}
		printf("La palabra ingresada es: %s\n", texto);

		for (; (ptrstart!=ptrend) && (ptrstart<ptrend); ptrstart++, ptrend--) {
			auxchar= *ptrstart, *ptrstart= *ptrend , *ptrend= auxchar;
		}
		printf("El texto invertido es: %s\n", texto);
	}
	return;
}

/* Ejercicio 3.2
 * Supóngase que se deposita una cantidad dada de dinero A en una cuenta de ahorros al principio de cada año
 * durante n años. Si se percibe un interés anual i, entonces la cantidad de dinero F que se acumulara tras n
 * años viene dada por...
 */
float calc_ganancia_anios(float capInicial, float interesAnual, int anios){
	int i=0;
	float acumCapital=0.0, interesCompuesto, acumuParcial=1.0;
	interesCompuesto= (1+interesAnual/100);

	for (; i < anios; ++i) {
		acumuParcial *= interesCompuesto;
		acumCapital += acumuParcial;
	}

	return acumCapital*capInicial;
}
float calc_capital_inicial(float capFinal, float interesAnual, int anios){
	int i=0;
	float acumCapital=0.0, interesCompuesto, acumuParcial=1.0;
	interesCompuesto= (1+interesAnual/100);

	for (; i < anios; ++i) {
		acumuParcial *= interesCompuesto;
		acumCapital += acumuParcial;
	}

	return capFinal/acumCapital;
}
void ejercicio_32_guia_1(void) {

	int anios= 0;
	float capInicial, capFinal; // U$S
	float interesAnual= 6.0; // % anual

	printf("Por favor ingrese el capital inicial (U$S) que quiere depositar:\n");
	scanf( "%10f" , &capInicial);
	printf("Por favor ingrese la cantidad de años:\n");
	scanf( "%10i" , &anios);
	printf("El capital acumuluada a un interes anual de %3.2f:\n (U$S) %.2f\n\n",
			interesAnual, calc_ganancia_anios(capInicial, interesAnual, anios));

	printf("Cuanto capital desea obtener:\n");
	scanf( "%10f" , &capFinal);
	printf("En que cantidad de años desea lograrlo:\n");
	scanf( "%10i" , &anios);
	printf("El capital constante que debe depositar año tras año un interes anual de %3.2f:\n (U$S) %.2f\n",
			interesAnual, calc_capital_inicial(capFinal, interesAnual, anios));


	return;
}



