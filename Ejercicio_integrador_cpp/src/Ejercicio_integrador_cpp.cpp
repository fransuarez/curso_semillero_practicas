//============================================================================
// Name        : Ejercicio_integrador_cpp.cpp
// Author      : Francisco
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
using namespace std;
#include <iostream>
#include <sstream>
#include <list>
#include <string>


#define MAX_INPUT 40
#define TOTAL_SENSORES 3
#define ID_TEMPERATURA 0
#define ID_HUMEDAD 1
#define ID_PRESION 2
#define DIA_AGRADABLE "Dia agradable con baja probabilidad de lluvias."
#define DIA_LLUVIOSO "Probabilidad baja de lluvias."
#define DIA_VARIABLE "Dia nuboso y vaiable."
#define VALOR_INICIAL 300.00

class Datos {
private:
	float valor_actual;
	float valor_minimo;
	float valor_maximo;
	float valor_prom;
	unsigned int numRegs;

public:
	Datos(){
		valor_actual=0;
		valor_minimo=0;
		valor_maximo=0;
		valor_prom= VALOR_INICIAL;
		numRegs= 0;
	}
	float get_actual(void){
		return this->valor_actual;
	}
	float get_promedio(void){
		return this->valor_prom;
	}
	float get_minimo(void){
		return this->valor_minimo;
	}
	float get_maximo(void){
		return this->valor_maximo;
	}
	void set_actual(float val){
		// FIXME deberia usar una condicion mejor para identificar el caso inicial.
		valor_actual= val;
		numRegs++;
		if(numRegs==0) numRegs=1; // FIXME Si se produce un overflow de la variable. Corregir

		if(valor_prom != VALOR_INICIAL){
			valor_prom+= val/numRegs;
			if(val > valor_maximo){
				valor_maximo=val;
			}
			if(val < valor_minimo){
				valor_minimo=val;
			}
		}
		else{
			this->valor_prom= val;
			this->valor_maximo= val;
			this->valor_minimo= val;
		}
	}
};

class Vistas {
public:
	virtual void imprimirDatos(Datos *val){
		return;
	}
};

class CondicionActual: public Vistas {
public:
	void imprimirDatos(Datos *val){
		cout << "1. Actual " << val[ID_TEMPERATURA].get_actual() << "°C y " << val[ID_HUMEDAD].get_actual() << "% de humedad.\n" << endl;
	}
};

class TemperaturaHitos: public Vistas {
private:
	float max, min, prom;
public:
	void imprimirDatos(Datos *val){
		max= val[ID_TEMPERATURA].get_maximo();
		min= val[ID_TEMPERATURA].get_minimo();
		prom= val[ID_TEMPERATURA].get_promedio();
		cout << "2. Temperatura Prom/Max/Min = "<< prom << "%.1f/"<< max <<"%.1f/"<< min <<"%.1f °C.\n" << endl;
	}
};

class PronosticoDia: public Vistas {
private:
	float temp, humedad, presion; // Solo para probar las tengo
public:
	void imprimirDatos(Datos *val){
		string salida;
		temp= val[ID_TEMPERATURA].get_actual();
		humedad= val[ID_HUMEDAD].get_actual();
		presion= val[ID_PRESION].get_actual();

		if(temp > 15 && humedad < 60){
			salida.append(DIA_AGRADABLE);
		}
		else if(presion < 30 && humedad > 60){
			salida.append(DIA_LLUVIOSO);
			salida = DIA_LLUVIOSO;
		}
		else {
			salida.append(DIA_LLUVIOSO);
		}
		cout << "3. Pronostico:" << salida << endl;
	}
};

class controladorVistas {
private:


public:
	list<Vistas*> misVistas;
	Datos *referencias;

	controladorVistas(Datos *ref){
		referencias= ref;
	}

	void actualizarVistas(){
		for (list<Vistas*>::iterator it=misVistas.begin(); it != misVistas.end(); ++it){
			(*it)->imprimirDatos(referencias);
		}
	}
	void agregarVista(Vistas *nuevaVista){
		this->misVistas.push_back(nuevaVista);
	}
	void quitarVista(Vistas *borrarVista){
		//this->misVistas.remove(borrarVista);
	}
};


#define COMANDO_SALIDA "q"
#define MENSAJE_BIENVENIDA "Bienvenido a la central del tiempo!!!\n Presione 'q' para salir."
#define PROMT_USUARIO "Ingrese (T[C°], H[\%], P[KPa]): "

class aplicacionClima {

private:
	string strconve;
	Datos temperatura;
	Datos presion;
	Datos humedad;
	char entrada[MAX_INPUT];

	void interpreteEntradas(float * salida, string entrada){
		istringstream validar(entrada);
		string valorTemp, valorHumedad, valorPresion;

		validar >> valorTemp >> valorHumedad >> valorPresion;

		//entrada.replace(entrada.begin(),entrada.end()," ");
		salida[ID_TEMPERATURA]=  stof(valorTemp, NULL);
		salida[ID_HUMEDAD]=  stof(valorHumedad, NULL);
		salida[ID_PRESION]=  stof(valorPresion, NULL);
	}

	void actualizarDatos(float ntemp, float npresion, float nhumedad){
		temperatura.set_actual(ntemp);
		presion.set_actual(npresion);
		humedad.set_actual(nhumedad);
	}

public:
	int servicio(float *sensores, Datos *parserValores){
		cout << PROMT_USUARIO;
		cin.getline(entrada, MAX_INPUT);

		strconve= string(entrada);
		if (!strconve.compare(COMANDO_SALIDA)){
			return 0;
		}
		else{
			this->interpreteEntradas(sensores, strconve);
			this->actualizarDatos(sensores[ID_TEMPERATURA], sensores[ID_HUMEDAD], sensores[ID_PRESION]);
		}
		return 1;
	}
};

int main() {
	float sensores[TOTAL_SENSORES];
	int retval;
	aplicacionClima aplicacion;
	Datos infoClima[TOTAL_SENSORES];
	TemperaturaHitos vista1_temp;
	PronosticoDia vista2_pronostico;
	CondicionActual vista3_actual;
	controladorVistas controlador(infoClima);

	// 0- Agrego o subscribo las vistas que deseo mostrar en pantalla.
	controlador.agregarVista(&vista1_temp);
	controlador.agregarVista(&vista2_pronostico);
	controlador.agregarVista(&vista3_actual);

	cout << MENSAJE_BIENVENIDA << endl;
	do{
	// 1- Realiza el pull de la informacion en los sensores ingresados.
		if((retval = aplicacion.servicio(sensores, infoClima))){
	// 2- Cuando hay informacion valida actualiza las vistas.
			controlador.actualizarVistas();
		}
	// 3- Si el usuario ingresa el comando de salida se interrumpe el loop.
	} while(retval);


	return 0;
}
